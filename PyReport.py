import datetime
import Constants
from Scrapper import Scrapper
from Mailer import *
from PieChart import PieChart
import Utils

class Record:
    def __init__(self, jobName, status, totalTests, failed, passed, lastBuild, notes):
        self.jobName = jobName
        self.status = status
        self.totalTests = totalTests
        self.failed = failed
        self.passed = passed
        self.lastBuild = lastBuild
        self.notes = notes

class PyReport:

    jobList = {}

    def __init__(self):
        self.totalTests = 0;
        self.passedTests = 0;
        self.failedTests = 0;

    def report(self, cellId, jobName, excelObj):
        print('wip!!')

    def initJobList(self):
        self.jobList = {Constants.ST_SARA, Constants.ST_ADMIN, Constants.ST_GENERAL, Constants.ST_SANITY}
        #self.jobList = {Constants.ST_SARA, Constants.ST_ADMIN, Constants.ST_GENERAL, Constants.ST_SANITY, Constants.ST_TOF, Constants.ST_POS, Constants.ST_EOS1, Constants.ST_XLINK, Constants.ST_TOF,
        #             Constants.ST_SYOMSALGO, Constants.ST_SYOMS, Constants.CT_STAS, Constants.CT_SAAPI, Constants.CT_PDD }

def main():
    print("Inside Main...")
    netTotalTests = 0
    netPassed = 0
    netFailed = 0
    testObj = PyReport()
    testObj.initJobList()
    mailObj = Mailer()
    recordList = []
    scrapObj = Scrapper(Constants.JENKINS_URL)

    for jobName in testObj.jobList :
        timestamp = None
        noteText = ""
        print("Job --> " +  jobName)
        jobScrapObj = Scrapper(Constants.JENKINS_URL + '/job/' + jobName)
        status = scrapObj.getStatusByJobId("job_" + jobName)
        lastBuild = (jobScrapObj.getLastBuild())
        resultList = jobScrapObj.getResutById()

        if status == "Failed" :
            #Failing Since details
            failingSinceObj = Scrapper(Constants.JENKINS_URL + '/job/' + jobName + '/' + str(jobScrapObj.getfailingSinceBuild()))
            details = failingSinceObj.getFailingSinceBuildDetails()
            timestamp = Utils.parseDatetimetoString(details.partition('(')[-1].rpartition(')')[0])
            noteText = Utils.generateNotes(timestamp, str(jobScrapObj.getfailingSinceBuild()) )

        #to be removed
        print("Total tests : " + str(resultList[0]))
        print("Failed tests : " + str(resultList[1]))
        print("Passed tests : " + str(resultList[2]))

        netTotalTests += int(resultList[0])
        netFailed += int(resultList[1])
        netPassed += int(resultList[2])
        recordList.append(Record(jobName, status, str(resultList[0]), str(resultList[1]), str(resultList[2]), lastBuild, noteText))

    pie = PieChart(netPassed, netFailed)
    pie.generateChart()
    mailObj.sendMail(recordList, netTotalTests, netPassed, netFailed)

if(__name__ == "__main__"):
    main()
    print("Execution Completed!")