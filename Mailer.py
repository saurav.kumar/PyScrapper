import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from PyReport import *

class Mailer:

    me = "saurav.kumar@iongroup.com"    #my email address
    you = "saurav.kumar@iongroup.com"  #recipient's email address

    def __init__(self):
        self.totalTests = 0;

    def initRows(self, rows):
        rowList = ''
        for i in range(0, len(rows)):
            percentagePassed = str(format(float(float(rows[i].passed) / float(rows[i].totalTests))*100, '.2f'))
            rowList = rowList + ("""<tr>
            <td class="tg-yw4l">""" + rows[i].jobName + """</td>
            <td class="tg-yw4l">""" + rows[i].status + """</td>
            <td class="tg-yw4l">""" + rows[i].totalTests + """</td>
            <td class="tg-yw4l">""" + rows[i].passed + """</td>
            <td class="tg-yw4l">""" + rows[i].failed + """</td>
            <td class="tg-yw4l">""" + percentagePassed + "%" + """</td>
            <td class="tg-yw4l"><a href=""" + Constants.JENKINS_URL  + "/job/" + rows[i].jobName + "/" +
                                 rows[i].lastBuild.replace("#", "") + ">" + rows[i].lastBuild + """</a></td>""" +
            self.returnFinalNotes(rows[i]) + """

          </tr>""")
        return rowList

    def returnFinalNotes(self, row):
        text = ""
        if(row.status) != "Success":
            text = """<td class="tg-yw4l">""" + Utils.getTextFromNote(row.notes) + """( Build #<a href=""" + Constants.JENKINS_URL  + "/job/" + row.jobName + "/" + Utils.getBuildNoFromNote(row.notes) + ">" + Utils.getBuildNoFromNote(row.notes) + """</a> )</td>"""
        else:
            text = """<td class="tg-yw4l"></td>"""

        return text

    def insertSummary(self, totalTests, passed, failed):
        summary = """<tr>
            <td class="tg-yw4l"> Summary </td>
            <td class="tg-yw4l"> </td>
            <td class="tg-yw4l">""" + str(totalTests) + """</td>
            <td class="tg-yw4l">""" + str(passed) + """</td>
            <td class="tg-yw4l">""" + str(failed) + """</td>
            <td class="tg-yw4l"> </td>
            <td class="tg-yw4l"> </td>
            <td class="tg-yw4l"> </td> </tr>
            """
        return summary

    def sendMail(self, reportList, netTotalTests, netPassed, netFailed):
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "PATS CORE Overnight Tests Report : " + datetime.datetime.now().strftime("%d-%m-%y")
        msg['From'] = self.me
        msg['To'] = self.you
        rowList = self.initRows(reportList)
        summary = self.insertSummary(netTotalTests, netPassed, netFailed)

        text = "this is demo text"
        # Create the body of the message (a plain-text and an HTML version).
        html = """\
        <html>
          <head></head>
          <body>
            <p>Hi All,<br><br>
               Please find below the summary of overnight tests with results.<br>
            </p>
            <br>
        <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
        .tm  {}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
        .tg .tg-yw4l{vertical-align:center}
        </style>
            <table class="tg">
              <tr>
                <th class="tg-yw4l">Job Name</th>
                <th class="tg-yw4l">Status</th>
                <th class="tg-yw4l">Total Tests</th>
                <th class="tg-yw4l">Passed</th>
                <th class="tg-yw4l">Failed</th>
                <th class="tg-yw4l">% Passed</th>
                <th class="tg-yw4l">Last Build</th>
                <th class="tg-yw4l">Notes</th>
              </tr> """ + rowList + summary + """   
              
            </table>
            <img src="C:\\Users\\Saurav.Kumar\\Desktop\\Python\\Scrapper\\piechart.png" class="tm"></img>
            <p><br> <br> Thanks,<br>
               Saurav<br>
            </p>
          </body>
        </html>
        """

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        # Send the message via local SMTP server.
        s = smtplib.SMTP('smtp.ffastconnect.com')
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        s.sendmail(self.me, self.you, msg.as_string())
        s.quit()