import matplotlib.pyplot as plt

class PieChart:

    def __init__(self, passed, failed):
        self.passed = passed;
        self.failed = failed;

    def generateChart(self):
        # Pie chart, where the slices will be ordered and plotted counter-clockwise:
        labels = 'Passed', 'Failed'
        sizes = [self.passed, self.failed]
        explode = (0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

        fig1, ax1 = plt.subplots()
        ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

        fig = plt.gcf()
        fig.set_size_inches(3,3)
        #plt.show()
        fig.savefig("piechart.png")