import bs4
import requests
import Constants

class Scrapper:

    def __init__(self, url):
        print "__init__"
        self.sendRequest(url)

    def sendRequest(self, url):
        self.res = requests.get(url)
        assert self.res.status_code == 200   #verify if connection is succesfull
        self.soup = bs4.BeautifulSoup(self.res.text, "lxml")    #lxlm parser
        #print(self.soup.prettify())

    def getStatusByJobId(self, jobId):
        html = self.soup.find_all(id=jobId)[0]
        result = html.findAll("img")[0]
        #print result.get('alt', '')
        return result.get('alt', '')

    def getResutById(self):
        resList = []
        html = self.soup.find_all(id="robot-summary-table")[0]
        result = html.findAll("td")

        for i in range(0,3):
            resList.append(result[i].text)

        return resList

    def getLastBuild(self):
        html = self.soup.find_all(id="buildHistory")[0]
        buildNo = html.findAll(True, {'class':['build-link']})[0]

        return str(buildNo.text)

    def getfailingSinceBuild(self):
        htmlLastSuccessfull = self.soup.find_all(id="main-panel")[0]
        buildNoLastSuccessfull = str(htmlLastSuccessfull.findAll(True, {'class': ['permalink-link']})[2].text)
        lastSuccessfullbuildNo = int(buildNoLastSuccessfull.partition('(')[-1].rpartition(')')[0].replace("#",""))
        failingSinceBuild = lastSuccessfullbuildNo + 1
        return failingSinceBuild

    def getFailingSinceBuildDetails(self):
        html = self.soup.find_all(id="main-panel")[0]
        buildNo = html.findAll(True, {'class': ['build-caption']})[0]
        return str(buildNo.text)

