from datetime import datetime

def parseDatetimetoString(stringDateTime):
    datetime_object = datetime.strptime(stringDateTime, '%d-%b-%Y %H:%M:%S')
    return datetime_object

def generateNotes(timestamp, buildNo):
    if timestamp != None:
        return "Failing since " + timestamp.strftime("%d-%m-%y") + ' ( #' + buildNo + ' )'
    else:
        return ""

def getBuildNoFromNote(note):
    return note.partition('(')[-1].rpartition(')')[0].replace("#","").replace(" ","")

def getTextFromNote(note):
    return note.split("(")[0]